Ce dossier contient les fonctions utilisées dans les programmes. 

- [F01_Fonctions_table_travail](/R/F01_Fonctions_table_travail.R) rassemble les fonctions utiles au nettoyage de la table prestation et à l'ajout des données issues des référentiels.

- [F02_Fonctions_extractions](/R/F02_Fonctions_extractions.R) rassemble les fonctions utiles à l'extraction des tables de données. 

- [F03_Code_commune_medecin](/R/F03_Code_commune_medecin.R) sert à la constitution et à la correction du code commune du cabinet du médecin.

- [F04_Code_commune_patient](/R/F04_Code_commune_patient.R) sert à la constitution et à la correction du code commune de résidence du patient.

- [F05_Standardisation](/R/F05_Standardisation.R) sert à corriger la part des téléconsultations dans l'activité des territoires de leur structure par âge.

# Copyright (C) 2022. Logiciel élaboré par l'État, via la Drees.
# Nom des auteur.e.s : Julie Kamionka et Maxime Bergeat, Drees.
# Ce programme informatique a été développé par la Drees.
# Il permet de produire les statistiques présentes dans l'Études et Résultats 1249, intitulé « Sept téléconsultations de médecine générale sur dix concernent des patients des grands pôles urbains en 2021 », décembre 2022.
# Ce programme a été exécuté en décembre 2022 avec la version 4.1.2 de R et les paquets data.table_1.14.2, dplyr_1.0.7, dbplyr_2.1.1 et ROracle_1.3-1.  
# La publication peut être consultée sur le site de la DREES : https://drees.solidarites-sante.gouv.fr/sites/default/files/2022-12/ER1249.pdf
# Bien qu'il n'existe aucune obligation légale à ce sujet, les utilisateurs de ce programme sont invités à signaler à la DREES leurs travaux issus de la réutilisation de ce code, ainsi que les éventuels problèmes ou anomalies qu'ils y rencontreraient, en écrivant à DREES-CODE@sante.gouv.fr
# Ce logiciel est régi par la licence EUPL V1.2 soumise au droit français et européen, respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence EUPL V1.2, telle que diffusée sur https://eupl.eu/1.2/fr/ et reproduite dans le fichier LICENCE diffusé conjointement au présent programme. 
# En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.
# À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

# Ce script rassemble les fonctions utiles à l'extraction des tables de données.

#### A. Extraction par mois de soin #### 

extraction_mois <- function(tab){
  tab %>%
    filter(PRS_NAT_REF %in% c(C_APL, V_APL, TLC_APL)) %>% # filtre consultations, visites, téléconsultations
    group_by(presta,EXE_SOI_AMD) %>%
    # type de prestation (consultation, visite, téléconsultation) et mois de soin
    summarize(quantite = sum(PRS_ACT_QTE,na.rm = T)) %>%
    collect
}

#### B. Extraction par médecin #### 

extraction_medecins <- function(tab){
  tab %>%
    filter(PRS_NAT_REF %in% c(C_APL, V_APL, TLC_APL)) %>% # filtre consultations, visites, téléconsultations
    group_by(IPP_SEX_COD, IPP_ANN_NAI, PFS_COD_POS, PFS_EXC_COM, PFS_LIB_COM, presta) %>%
    # sexe du médecin, année de naissance du médecin, variables pour code commune du médecin
    # et type de prestation (consultation, visite, téléconsultation)
    summarize(quantite = sum(PRS_ACT_QTE,na.rm = T)) %>%
    collect
}

#### C. Extraction par patient ####

extraction_patients <- function(tab){
  tab %>%
    filter(PRS_NAT_REF %in% c(C_APL, V_APL, TLC_APL)) %>% # filtre consultations, visites, téléconsultations
    group_by(BEN_SEX_COD, BEN_NAI_ANN, BEN_RES_DPT, BEN_RES_COM, ORG_AFF_BEN, BEN_CSS, presta) %>%
    # sexe du patient, année de naissance du patient, variables pour code commune du patient 
    # variable patient CSS et type de prestation (consultation, visite, téléconsultation)
    summarize(quantite = sum(PRS_ACT_QTE,na.rm = T)) %>%
    collect
}

##### Extraction patients - médecins traitants ##### 

extraction_mtt <- function(tab){
  tab %>%
    filter(PRS_NAT_REF %in% c(C_APL, V_APL, TLC_APL) & STA_PFS_NUM == STA_PFS_NUM_MTT) %>%  
    # filtre consultations, visites, téléconsultations + médecin traitant
    group_by(presta) %>% # type de prestation (consultation, visite, téléconsultation)
    summarize(quantite = sum(PRS_ACT_QTE,na.rm = T)) %>%
    collect
}

##### Extraction patients en centre de santé #####

extraction_cds <- function(tab){
  tab %>%
    filter(PRS_NAT_REF %in% c(C_APL, V_APL, TLC_APL)) %>% # filtre consultations, visites, téléconsultations
    group_by(BEN_NAI_ANN, BEN_RES_DPT, BEN_RES_COM, ORG_AFF_BEN, presta) %>%
    # année de naissance du patient, variable pour code commune du patient
    # et type de prestation (consultation, visite, téléconsultation)
    summarize(quantite = sum(PRS_ACT_QTE,na.rm = T)) %>%
    collect
}

#### D. Extraction localisation patients x médecins ####

extraction_patients_medecins <- function(tab){
  tab %>%
    filter(PRS_NAT_REF %in% c(C_APL,V_APL,TLC_APL)) %>% # filtre consultations, visites et téléconsultations
    group_by(BEN_RES_DPT,BEN_RES_COM,ORG_AFF_BEN,PFS_COD_POS,PFS_EXC_COM,PFS_LIB_COM,presta) %>% 
    # variables pour construire le code commune du patient et celui du médecin 
    # + type de prestation (consultation, visite, téléconsultation)
    summarize(nb = sum(PRS_ACT_QTE,na.rm = T)) %>%
    collect
}

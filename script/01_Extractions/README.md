Ce dossier contient les programmes d'extraction des données utilisées dans l'étude.

- [E01_Extraction_mois_de_soin](/script/01_Extractions/E01_Extraction_mois_de_soin.R) sert à l'extraction des données par mois de soin des médecins généralistes libéraux et salariés en centre de santé (graphique 1 et graphique de l'encadré 2 de l'étude).

- [E02_Extraction_medecins](/script/01_Extractions/E02_Extraction_medecins.R) fait l'extraction des données qui servent aux analyses sur les médecins généralistes libéraux.

- [E03_Extraction_patients](/script/01_Extractions/E03_Extraction_patients.R) fait l'extraction des données qui servent aux analyses sur les patients des médecins généralistes libéraux et salariés en centre de santé.

- [E04_Extraction_localisation_patients_medecins](/script/01_Extractions/E04_Extraction_localisation_patients_medecins.R) fait l'extraction des données de localisation des patients et des médecins généralistes libéraux qui servent aux analyses sur les distances patient-médecin.

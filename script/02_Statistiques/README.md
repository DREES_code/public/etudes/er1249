Ce dossier contient les programmes qui produisent les statistiques de l'étude à partir des données extraites.

- [S01_Statistiques_medecins](/script/02_Statistiques/S01_Statistiques_medecins.R) produit les analyses sur les médecins généralistes libéraux.

- [S02_Statistiques__medecins_patients](/script/02_Statistiques/S02_Statistiques_patients.R) produit les analyses sur les patients des médecins généralistes libéraux.

- [S03_Localisation_patients_medecins](/script/02_Statistiques/S03_Localisation_patients_medecins.R) produit les analyses sur les distances entre un patient et son médecin généraliste libéral.

- [S04_Centres_de_sante](/script/02_Statistiques/S04_Centres_de_sante.R) produit les analyses sur les patients des médecins généralistes salariés en centre de santé.

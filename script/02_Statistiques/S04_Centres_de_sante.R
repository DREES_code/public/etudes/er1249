# Copyright (C) 2022. Logiciel élaboré par l'État, via la Drees.
# Nom des auteur.e.s : Julie Kamionka et Maxime Bergeat, Drees.
# Ce programme informatique a été développé par la Drees.
# Il permet de produire les statistiques présentes dans l'Études et Résultats 1249, intitulé « Sept téléconsultations de médecine générale sur dix concernent des patients des grands pôles urbains en 2021 », décembre 2022.
# Ce programme a été exécuté en décembre 2022 avec la version 4.1.2 de R et les paquets data.table_1.14.2, dplyr_1.0.7, dbplyr_2.1.1 et ROracle_1.3-1.  
# La publication peut être consultée sur le site de la DREES : https://drees.solidarites-sante.gouv.fr/sites/default/files/2022-12/ER1249.pdf
# Ce programme utilise les données du SNIIRAM (CNAM) et la typologie communale issue du Dossier de la Drees 63 « Appréhender les territoires ruraux dans les études de la DREES », N. Missègue, juillet 2020(https://drees.solidarites-sante.gouv.fr/sites/default/files/2020-10/dd63.pdf) en géographie 2022 (DREES).
# Bien qu'il n'existe aucune obligation légale à ce sujet, les utilisateurs de ce programme sont invités à signaler à la DREES leurs travaux issus de la réutilisation de ce code, ainsi que les éventuels problèmes ou anomalies qu'ils y rencontreraient, en écrivant à DREES-CODE@sante.gouv.fr
# Ce logiciel est régi par la licence EUPL V1.2 soumise au droit français et européen, respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence EUPL V1.2, telle que diffusée sur https://eupl.eu/1.2/fr/ et reproduite dans le fichier LICENCE diffusé conjointement au présent programme. 
# En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.
# À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

# Ce script produit les analyses sur les patients des médecins généralistes salariés en centre de santé.

rm(list=ls())

path_scripts <- "/script/"

#### Initialisation ####
source(paste0(path_scripts,"00_Initialisation/I00_Initialisation.R"))

#### Import des données complémentaires ####

# Communes 2022
com2022 <- fread(paste0(path_import,"communes2022.csv"))
setDT(com2022)

# Typologie des communes N.Missègue DD63 (géo 2022)
typo <- fread(paste0(path_import,"typo_communes_drees2022.csv")) 
setDT(typo)

#### Import des données par patient des centres de santé ####

donnees_patients_cs <- fread(paste0(path_sorties,jour_extractions,"_table_patients_cs_code_comm_corrige.csv"))

#### A. Répartition de l'activité selon l'âge du patient en 2021 ####

# Année de naissance renseignée
donnees_patients_cs[,BEN_NAI_ANN := as.numeric(BEN_NAI_ANN)]
donnees_patients_cs[BEN_NAI_ANN < 1903,BEN_NAI_ANN := NA]

# Age du patient
donnees_patients_cs[,age2021 := 2021 - BEN_NAI_ANN]
act_agepatients <- donnees_patients_cs[!is.na(age2021) & age2021 >= 0,]

# Tranches d'âge
tr_ages_pat=c(0,15,30,45,60,75,Inf)
act_agepatients[,tr_age_2021 := cut(age2021, breaks = tr_ages_pat, include.lowest = T,right = F)]

# Consultations
act_agepatients %>% 
  filter(presta == "C") %>% 
  group_by(tr_age_2021) %>% 
  summarise(quantite2021 = sum(quantite2021,na.rm = T)) %>% 
  mutate(prop = quantite2021/sum(quantite2021,na.rm = T))

# Visites
act_agepatients %>% 
  filter(presta == "V") %>% 
  group_by(tr_age_2021) %>% 
  summarise(quantite2021 = sum(quantite2021,na.rm = T)) %>% 
  mutate(prop = quantite2021/sum(quantite2021,na.rm = T))

# Téléconsultations
act_agepatients %>% 
  filter(presta == "TLC") %>% 
  group_by(tr_age_2021) %>% 
  summarise(quantite2021 = sum(quantite2021,na.rm = T)) %>% 
  mutate(prop = quantite2021/sum(quantite2021,na.m = T))

# Taux d'activité avec âge retrouvé
sum(act_agepatients$quantite2021, na.rm = T)/sum(donnees_patients_cs$quantite2021, na.rm = T) # > 99.9 % en 2021

#### B. Répartition de l'activité selon la typologie de la commune de résidence du patient en 2021 #####

# Typologie communale
donnees_patients_cs <- merge(donnees_patients_cs,typo,by.x = "depcom_ben",by.y = "INSEE_COM",all.x = T)
donnees_patients_cs[depcom_ben=="14666", ruralite_villes_niv3 := "31. Territoires ruraux des grandes aires"]
donnees_patients_cs[depcom_ben=="14666", ruralite_villes_niv2 := "31. Territoires ruraux des grandes aires"]
donnees_patients_cs[depcom_ben=="14666", ruralite_villes_niv1 := "3. Territoires ruraux des grandes, moyennes, petites aires et isolés"]
donnees_patients_cs[depcom_ben=="92012", ruralite_villes_niv3 := "112. Banlieues du pôle urbain de Paris"]
donnees_patients_cs[is.na(ruralite_villes_niv3) & substr(depcom_ben,1,2) == "97", ruralite_villes_niv3 := "Outre-mer (hors Mayotte)"]
donnees_patients_cs[is.na(ruralite_villes_niv2) & substr(depcom_ben,1,2) == "97", ruralite_villes_niv2 := "Outre-mer (hors Mayotte)"]
donnees_patients_cs[is.na(ruralite_villes_niv1) & substr(depcom_ben,1,2) == "97", ruralite_villes_niv1 := "Outre-mer (hors Mayotte)"]

act_typoPat <- donnees_patients_cs[!is.na(ruralite_villes_niv3),]

data_enc2 <- act_typoPat %>% group_by(presta, ruralite_villes_niv3) %>% 
  mutate(prop = quantite2021/sum(quantite2021,na.rm = T))

# Taux d'activité avec typologie communale retrouvée
sum(data_enc2$quantite2021, na.rm = T)/sum(donnees_patients_cs$quantite2021, na.rm = T) # 98.6 % en 2021

- [I00_Initialisation](/script/00_Initialisation/I00_Initialisation.R) est le script d'initialisation.

- [I01_Table_de_travail](/script/00_Initialisation/I01_Table_de_travail.R) est le script qui nettoie la table des prestations et y ajoute les données issues des référentiels.

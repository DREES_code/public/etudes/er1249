# ER1249

Ce dossier fournit les programmes ayant servi à produire l'*Études et Résultats* N° 1249 de la Drees : [Sept téléconsultations de médecine générale sur dix concernent des patients des grands pôles urbains en 2021](https://drees.solidarites-sante.gouv.fr/sites/default/files/2022-12/ER1249.pdf), publié en décembre 2022.

La [Direction de la recherche, des études, de l'évaluation et des statistiques](https://drees.solidarites-sante.gouv.fr/article/presentation-de-la-drees) (Drees) est le service statistique ministériel des ministères sanitaires et sociaux, et une direction de l'administration centrale de ces ministères.

Source des données : Les programmes présents dans ce dossier mobilisent les données du SNIIRAM (CNAM) et le distancier Metric (INSEE).

Les programmes ont été exécutés pour la dernière fois en décembre 2022 avec le logiciel R version 4.1.2.
